(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .controller('OrdersController', OrdersController);

    function OrdersController($http, $location, $state, orders, orderId, order, timologiaService) {
        var ordersCtrl = this;

        ordersCtrl.orders = orders;


        if (order){
            ordersCtrl.order = order;
            ordersCtrl.orderId = orderId;
            ordersCtrl.comesFromPost = $location.search().submit !== undefined;
        }

        ordersCtrl.getTotal = function(item){
            return ((item.price_in_order * item.quantity).toFixed(2));
        };

        ordersCtrl.getSynolikiAksiaFarmakwnPFS = function(){
            var sum = 0;
            var l = ordersCtrl.order.length;
            for (var i=0; i<l; i++){

                var item = ordersCtrl.order[i];
                if (item.apothiki === "1"){
                    
                    sum += item.price_arxiki * item.quantity;

                }else if (item.apothiki === "0"){
                    sum += 0;
                }

            }

            return sum;
            
        };

        ordersCtrl.getSynolikiAksiaParaggelias = function(){
            var sum = 0;
            var l = ordersCtrl.order.length;
            for (var i=0; i<l; i++){
                var item = ordersCtrl.order[i];
                var q = (item.price_in_order * item.quantity).toFixed(2);
                sum = sum + parseFloat(q);
            }
            return sum.toFixed(2);
        };

        ordersCtrl.getPfs = function(){
            var pfs = ordersCtrl.getSynolikiAksiaFarmakwnPFS();
            return (pfs * (4/1000)).toFixed(2);
        };

        ordersCtrl.getPfsFPA = function(){
            var fpaPFS = ordersCtrl.getPfs();
            return (fpaPFS * (6/100)).toFixed(2);
        };

        ordersCtrl.getFPA = function(){
            var fpa = {
                fpa_6: 0,
                fpa_13: 0,
                fpa_24: 0
            };
            var l = ordersCtrl.order.length;
            for (var i=0; i<l; i++){
                var item = ordersCtrl.order[i];
                switch(item.fpa_code){
                    case "1":
                        fpa.fpa_6 += item.price_in_order * item.quantity * (6/100);
                        break;
                    case "7":
                        fpa.fpa_13 += item.price_in_order * item.quantity * (13/100);
                        break;
                    case "8":
                        fpa.fpa_24 += item.price_in_order * item.quantity * (24/100);
                        break;
                    
                }
            }

            fpa.fpa_total = (fpa.fpa_13 + fpa.fpa_24 + fpa.fpa_6).toFixed(3);

            return fpa;
        };

        ordersCtrl.total = function(){
            return  (parseFloat(ordersCtrl.getSynolikiAksiaParaggelias()) + 
                    parseFloat(ordersCtrl.getPfs()) + 
                    parseFloat(ordersCtrl.getPfsFPA()) + 
            parseFloat(ordersCtrl.getFPA().fpa_total)).toFixed(2);
        };
        

        ordersCtrl.getTimologio = function(id){

            timologiaService.getTimologio(id).then(function (response) {
                var blob = new Blob([response.data], { type: "text/plain;charset=windows-1253" });
                saveAs(blob, `b2b_${id}.txt`);
            },
            function (error) {
                console.error(error);
            });

        };

        
    }

})();