(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .controller('LoginController', LoginController);

    function LoginController($location, $state, $stateParams, userService) {
        var loginCtrl = this;


        function handleRequest(res){
            loginCtrl.dataLoading = false;
            if (res.data.status === "error"){
                loginCtrl.errorMsg = res.data.message;
            }else {
                $location.path('/');
            }
        }


        loginCtrl.id = $stateParams.id;
        loginCtrl.message = "login here";

        loginCtrl.login = function(){
            
            loginCtrl.errorMsg = null;
            loginCtrl.dataLoading = true;
            
            userService.login(loginCtrl.email, loginCtrl.password)
                .then(handleRequest, handleRequest);
        }

        
    }

})();