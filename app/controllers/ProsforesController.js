(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .controller('ProsforesController', ProsforesController);

    function ProsforesController($location, prosfores) {
        var prosforesCtrl = this;

        prosforesCtrl.prosfores = prosfores;

    }

})();

