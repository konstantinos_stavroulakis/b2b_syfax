(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .controller('RegisterController', RegisterController);

    function RegisterController($location, $document, $scope, userService, doy) {
        var registerCtrl = this;

        registerCtrl.message = "register here";
        registerCtrl.isSyfaxUser = true;
        registerCtrl.doy = doy;

        function handleRequest(res){
            
            registerCtrl.dataLoading = false;
            registerCtrl.status = res.data.status === "success";
            if (res.data.status === "success"){
                registerCtrl.msg = "Η εγγραφή του χρήστη έγινε επιτυχώς. Θα πρέπει να ενεργοποιηθείτε από το διαχειριστή της εφαρμογής.";
            }else {
                registerCtrl.msg = "Η εγγραφή του χρήστη απέτυχε. Παρακαλώ επικοινωνήσετε με το διαχειριστή της εφαρμογής.";
            }

            registerCtrl.user = {}; // or new User() if it is your case
            $scope.form.$setPristine();
        }

        registerCtrl.setTab = function(isSyfax){
            //if (isSyfax == false) return;
            registerCtrl.isSyfaxUser = isSyfax;
            
            var tabSyfax = $document[0].getElementById('tab_syfax');
            var tabOther = $document[0].getElementById('tab_other');

            if (!isSyfax){

                

                // go to others to syfax tab
                if (tabSyfax.classList.contains('active')){
                    tabSyfax.classList.remove('active');
                    tabOther.classList.add('active');
                }
                
            }else{
                // go to syfax tab
                if (tabOther.classList.contains('active')){
                    tabOther.classList.remove('active');
                    tabSyfax.classList.add('active');
                }
            }
            
            
        };

        registerCtrl.isSyfax = function(){
            return registerCtrl.isSyfaxUser;
        };

        registerCtrl.register = function(){
            registerCtrl.dataLoading = true;
            userService.register(registerCtrl.user)
                .then(handleRequest, handleRequest);
        };
    }

})();

