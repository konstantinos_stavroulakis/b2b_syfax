(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .controller('MainController', MainController);

    function MainController($http, $location, $state, $stateParams, authService, flashService) {
        var mainCtrl = this;

        mainCtrl.currentMessage = $stateParams.currentMessage;


        mainCtrl.isLoggedIn = function(){
            return authService.isAuthed();
        }

        mainCtrl.logout = function(){
            authService.logout();
            $http.defaults.headers.common.Authorization = '';
            $state.go('login');

        }

        mainCtrl.getUserData = function(){
            var token = authService.getToken();
            return authService.parseJwt(token).data;
        }

        
    }

})();