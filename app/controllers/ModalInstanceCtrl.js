(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .controller('ModalInstanceCtrl', ModalInstanceCtrl);

    function ModalInstanceCtrl($uibModalInstance, item, $window, $location, ordersService ) {
        var $ctrl = this;
        $ctrl.item = item;
        

        $ctrl.ok = function () {
            
            var ret = ordersService.cancelOrder(item )
            .then(function (data, status, headers, conf) {                    
                if (data.data.result.success){

                    
                    
                    $window.location.reload();

                }else {

                }
            }).finally(function () {
                $uibModalInstance.close($ctrl.item);
            });



            
        };

        $ctrl.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
        
    }

})();


