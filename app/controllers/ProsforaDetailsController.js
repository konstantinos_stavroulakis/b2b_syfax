(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .controller('ProsforaDetailsController', ProsforaDetailsController);

    function ProsforaDetailsController($scope, $filter, $document, $location, prosfora, libProsforaService, prosforesService, flashService, $sce, authService ) {
        var prosforaDetailsCtrl = this;

        prosforaDetailsCtrl.cart = {
            items:[],
            invalids:[],
            totalCost: function(){
                var total = 0;
                var cart = this;


                angular.forEach(this.items, function (item) {
                    
                    total += cart.getQuantity(item) * item.price_discount;
                });
                return +parseFloat(total).toFixed(2);

            },
            getQuantity: function(item){
                return (!angular.isUndefined(item.new_quantity) && item.new_quantity !== "" && item.new_quantity !== null ? item.new_quantity : 0);
            },
            addToCart: function(item){
                if (!this.isInCart(item)){
                    if (!this.hasMinAndValueZero(item))
                        this.items.push(item);

                }else {
                    // check if to update quantity or not
                    if (this.hasMinAndValueZero(item))
                        this.removeFromCart(item.item_id);
                }
            },
            totalProioda: function(){
                var count = 0;
                for(var i=0; i<this.items.length; i++){
                    count += parseInt(this.items[i].new_quantity);
                }
                return count;
            },
            hasMinAndValueZero: function(item){
                return parseInt(item.min_quantity) === 0 && parseInt(item.new_quantity) === 0;
            },
            isInCart: function(item){

                if (!this.getById(item.item_id)){
                    return false;
                }else {
                    return true;
                }

            },
            isInInvalids: function(item){

                if (!this.getById(item.item_id)){
                    return false;
                }else {
                    return true;
                }

            },
            removeFromCart: function(item_id){
                for(var i=0 ; i<this.items.length; i++){
                    if (this.items[i].item_id === item_id) {
                        this.items.splice(i, 1);
                        break;
                    }
                }
            },
            removeFromInvalids: function(item_id){
                for(var i=0 ; i<this.invalids.length; i++){
                    if (this.invalids[i].item_id === item_id) {
                        this.invalids.splice(i, 1);
                        break;
                    }
                }
            },
            getById: function(id){
                var theItem = false;

                angular.forEach(this.items, function (item) {
                    if  (item.item_id === id) {
                        theItem = item;
                    }
                });

                return theItem;

            },
            getInvalidById: function(id){
                var theItem = false;

                angular.forEach(this.invalids, function (item) {
                    if  (item.item_id === id) {
                        theItem = item;
                    }
                });

                return theItem;

            }
        };

        prosforaDetailsCtrl.prosforaGroups = null;
        prosforaDetailsCtrl.prosforaGroupsKeys = null;

        prosforaDetailsCtrl.userIsMelos = authService.isUserMelos();

        prosforaDetailsCtrl.acceptTerms = false;
        prosforaDetailsCtrl.checked = false;
        prosforaDetailsCtrl.currentPage = 1;
        prosforaDetailsCtrl.pageSize = 10;
        prosforaDetailsCtrl.itemsPerPage = "10";
        prosforaDetailsCtrl.itemCodeFilter = '';
        prosforaDetailsCtrl.itemNameFilter = '';

        var disCounts = libProsforaService.getDiscountOpts(parseInt(prosfora.category));
        if (prosfora.category === "23" || prosfora.category === "24" || prosfora.category === "27"|| prosfora.category === "29"|| prosfora.category === "30"|| prosfora.category === "31"|| prosfora.category === "32"|| prosfora.category === "35"|| prosfora.category === "36"|| prosfora.category === "37"){
            prosforaDetailsCtrl.discounts = disCounts.filter(function(item){
                return item.memberOnly === null || item.memberOnly === undefined || item.memberOnly === prosforaDetailsCtrl.userIsMelos;
            });
        }else {
            prosforaDetailsCtrl.discounts = disCounts;
        }
        
        prosforaDetailsCtrl.discountValue = prosforaDetailsCtrl.discounts.length > 0 ? prosforaDetailsCtrl.discounts[0].value : 0 ;

        prosforaDetailsCtrl.changed = function($event){
            console.log($event)
            var keyCode = $event.keyCode;
            
            if(!((keyCode > 95 && keyCode < 106)
                || (keyCode > 47 && keyCode < 58) 
                || keyCode == 8)) {
                    $event.preventDefault();
            }
        }
        
        // if we have one prosfora
        if (prosfora){
            
            
            prosforaDetailsCtrl.category = prosfora.category;
            // always set title
            prosforaDetailsCtrl.prosfora_title = prosfora.prosfora_title;

            prosforaDetailsCtrl.comment = prosfora.prosfora_comment || "";

            // if is a prosfora with category 8, group per item_id
            // pick groups, and build discount rules
            // show only one row per group
            if (prosfora.category === "8"||prosfora.category === "39"){
                prosforaDetailsCtrl.prosforaGroups = groupBy(prosfora.items, "item_id");
                prosforaDetailsCtrl.prosforaGroupsKeys = Object.keys(prosforaDetailsCtrl.prosforaGroups);

                prosforaDetailsCtrl.items = [];
                for (var g=0; g<prosforaDetailsCtrl.prosforaGroupsKeys.length; g++){

                    var groupItems = prosforaDetailsCtrl.prosforaGroups[prosforaDetailsCtrl.prosforaGroupsKeys[g]];
                    var str = "";
                    var options = [];
                    groupItems.forEach(function(item){
                        str += "Ποσότητα: " + item.min_quantity + " Έκπτωση: " + item.item_ekptwsi + "%<br>";
                        options.push({q:item.min_quantity, d:item.item_ekptwsi});
                    });


                    prosforaDetailsCtrl.items.push(prosforaDetailsCtrl.prosforaGroups[prosforaDetailsCtrl.prosforaGroupsKeys[g]][0]);
                    prosforaDetailsCtrl.items[g].groupDiscountStr = $sce.trustAsHtml(str);
                    prosforaDetailsCtrl.items[g].groupDiscounts = options;
                    

                    console.table(prosforaDetailsCtrl.items);
                }

            }else if (prosfora.category === "10"){

                prosforaDetailsCtrl.prosforaGroups = groupBy(prosfora.items, "min_quantity");
                prosforaDetailsCtrl.prosforaGroupsKeys = Object.keys(prosforaDetailsCtrl.prosforaGroups);
                prosforaDetailsCtrl.firstAfterMin = false;

                prosforaDetailsCtrl.prosforaDiscountStr = '<p style="width:340px">';
                prosforaDetailsCtrl.prosforaDiscountOptions = [];
                prosforaDetailsCtrl.items = [];
                for (var g=0; g<prosforaDetailsCtrl.prosforaGroupsKeys.length; g++){

                    
                    var groupItems = prosforaDetailsCtrl.prosforaGroups[prosforaDetailsCtrl.prosforaGroupsKeys[g]];
                    
                    groupItems.forEach(function(item){ 
                        item.min_quantity = 0;
                        //str += "Ποσότητα: " + item.min_quantity + " Έκπτωση: " + item.item_ekptwsi + "%<br>";
                        
                    });

                    prosforaDetailsCtrl.prosforaDiscountOptions.push(
                        {
                            q:prosforaDetailsCtrl.prosforaGroupsKeys[g], 
                            d:groupItems[0].item_ekptwsi
                        }
                    );
                    prosforaDetailsCtrl.prosforaDiscountStr += $sce.trustAsHtml("Αθροιστική Ποσότητα:" +  prosforaDetailsCtrl.prosforaGroupsKeys[g] + " Έκπτωση: " + groupItems[0].item_ekptwsi + "%<br>");
                    
                    //prosforaDetailsCtrl.items.push(prosforaDetailsCtrl.prosforaGroups[prosforaDetailsCtrl.prosforaGroupsKeys[g]]);
                    //prosforaDetailsCtrl.items[g].groupDiscountStr = $sce.trustAsHtml(str);
                    //prosforaDetailsCtrl.items[g].groupDiscounts = options;
                    

                    
                }
                prosforaDetailsCtrl.prosforaDiscountStr += "</p>";
                prosforaDetailsCtrl.items = prosforaDetailsCtrl.items.concat(prosforaDetailsCtrl.prosforaGroups[prosforaDetailsCtrl.prosforaGroupsKeys[0]]);

                //console.table(prosforaDetailsCtrl.items);
                //console.log(prosforaDetailsCtrl.prosforaGroupsKeys);
                
            }
            else {
                prosforaDetailsCtrl.items = prosfora.items;
            }
            
            // kleidwmeno
            if (prosfora.prosfora_type === "1"){
                
            }

            // do some data initializations before viewing
            var l = prosforaDetailsCtrl.items.length;
            

            while (l--){
                
                if (prosforaDetailsCtrl.items[l].min_quantity > 0){
                    // if item's min quantity is not 0, then set quantity to min quantity and add to cart by default
                    prosforaDetailsCtrl.items[l].new_quantity = prosforaDetailsCtrl.items[l].min_quantity;
                    prosforaDetailsCtrl.cart.addToCart(prosforaDetailsCtrl.items[l]);
                }else{
                    // else set quantity to null
                    prosforaDetailsCtrl.items[l].new_quantity = 0;
                }
                
            }
            
            // apply items the discount. global or each
            var prosforaCategory = parseInt(prosfora.category);
            var seperate = prosforaCategory === 6 || prosforaCategory === 8 || prosforaCategory === 39 ? true : false;
            applyDiscount(prosforaDetailsCtrl.discountValue, seperate );
    
        }

        prosforaDetailsCtrl.getDiscountText = function(text){
            return $sce.trustAsHtml(text);
        };

        prosforaDetailsCtrl.prosfora = prosfora;
        
        
        prosforaDetailsCtrl.getData = function(){

            if (prosforaDetailsCtrl.itemNameFilter !== "" || prosforaDetailsCtrl.itemCodeFilter !== ""){
                //prosforaDetailsCtrl.currentPage = 0;
            }

            return $filter('filter')(
                    prosforaDetailsCtrl.items,
                    { item_name: prosforaDetailsCtrl.itemNameFilter, item_code: prosforaDetailsCtrl.itemCodeFilter }
                );
        };

        prosforaDetailsCtrl.numberOfPages = function(){
            return Math.ceil(prosforaDetailsCtrl.getData().length/prosforaDetailsCtrl.pageSize);
        };

        angular.element('#confirm-submit').on('shown.bs.modal', function(e){
            prosforaDetailsCtrl.acceptTerms = false;
            //angular.element("#terms_check").attr("checked", false);
        });

        angular.element('#confirm-submit').on('hidden.bs.modal', function(e){
            prosforaDetailsCtrl.acceptTerms = false;
            angular.element("#terms_check").attr("checked", false);
        });
        

        prosforaDetailsCtrl.toggleSelection = function($event, item){
            var checkbox = $event.target;
            prosforaDetailsCtrl.acceptTerms = checkbox.checked;
        };


        prosforaDetailsCtrl.submitForm = function(theForm, isPending){
            
            prosforaDetailsCtrl.dataLoading = true;
            var pendingOrder = (isPending && isPending === true) ? true : false;

            var sendData = [];
            var l = prosforaDetailsCtrl.cart.items.length;
            while (l--){
                sendData.push({
                    item_id: prosforaDetailsCtrl.cart.items[l].item_id,
                    min_quantity:prosforaDetailsCtrl.cart.items[l].min_quantity,
                    new_quantity:prosforaDetailsCtrl.cart.items[l].new_quantity,
                    price_discount:prosforaDetailsCtrl.cart.items[l].price_discount,
                    price_arxiki:prosforaDetailsCtrl.cart.items[l].price_katharo,
                    item_ekptwsi:prosforaDetailsCtrl.cart.items[l].item_ekptwsi
                });
            }
            
            var ret = prosforesService.sendOrder(sendData, prosforaDetailsCtrl.prosfora.id, prosforaDetailsCtrl.discountValue, 
            (parseInt(prosfora.category) === 6 || parseInt(prosfora.category) === 8 || parseInt(prosfora.category) === 10 || parseInt(prosfora.category) === 39 ) ? 1 : 0, pendingOrder )
            .then(function (data, status, headers, conf) {                    
                if (data.data.result.status == "success"){

                    if (!pendingOrder){
                        var newOrderId = data.data.result.orderid;
                        $location.path('summary/'+newOrderId).search({submit: 'true'});
                        angular.element('#confirm-submit').modal('hide');
                        $document[0].body.classList.remove('modal-open');
                        angular.element($document[0].getElementsByClassName('modal-backdrop')).remove();
                        angular.element($document[0].getElementsByClassName('modal')).remove();
                    }else {
                        var newPendingOrderId = data.data.result.orderid;
                        flashService.set("Η προσφορά με κωδικό #" + newPendingOrderId + " μπήκε σε αναμονή");
                        $location.path('/');
                        $document[0].body.classList.remove('modal-open');
                        angular.element($document[0].getElementsByClassName('modal-backdrop')).remove();
                        angular.element($document[0].getElementsByClassName('modal')).remove();

                    }


                }else {

                }
            }).finally(function () {
                prosforaDetailsCtrl.dataLoading = false;
            });
            
        };




        prosforaDetailsCtrl.addItem = function(item, elem, valid){
            var tip = angular.element("#item_tip"+item.item_id)[0];
            if (item.new_quantity >= 100)
                tip.style.display = "block";
            else
                tip.style.display = "none";

            if (valid){
                if (prosforaDetailsCtrl.cart.getInvalidById(item.item_id))
                    prosforaDetailsCtrl.cart.removeFromInvalids(item.item_id);
                
                // if valid
                // if category is 8 then, change the discount by the groups quantity
                if (prosfora.category === "8" || prosfora.category === "39"){
                    var q = parseInt(item.new_quantity);
                    //console.log(item.groupDiscounts);
                    var groupDiscountSelected = null;

                    for (var d = 0; d<item.groupDiscounts.length; d++){
                        
                        if (d === item.groupDiscounts.length -1){
                            //console.log(parseInt(item.new_quantity));
                            //console.log(parseInt(item.groupDiscounts[d].d));
                            groupDiscountSelected = parseFloat(item.groupDiscounts[d].d);
                            break;
                        }else {
                            if (parseInt(item.new_quantity) >= parseInt(item.groupDiscounts[d].q) && parseInt(item.new_quantity) < parseInt(item.groupDiscounts[d+1].q) ){
                                groupDiscountSelected = parseFloat(item.groupDiscounts[d].d);
                                break;
                            }
                        }
                        
                    }

                    //console.log(groupDiscountSelected);
                    
                    item.price_discount = (item.price_katharo * ((100-parseFloat(groupDiscountSelected)) / 100)).toFixed(2);
                    item.item_ekptwsi = groupDiscountSelected;
                
                }else if (prosfora.category === "10"){

                    if (item.new_quantity === undefined) item.new_quantity = 0;
                    var item_q = parseInt(item.new_quantity);

                    var total = 0;
                    var groupDiscountSelected = null;
                    prosforaDetailsCtrl.items.forEach(function(item){
                        total += parseInt(item.new_quantity);
                    });

                    //console.log(total);
                    //console.log(prosforaDetailsCtrl.prosforaGroups);
                    //console.log(prosforaDetailsCtrl.prosforaGroupsKeys);

                    for (var d = 0; d<prosforaDetailsCtrl.prosforaGroupsKeys.length; d++){
                        
                        if (d === prosforaDetailsCtrl.prosforaGroupsKeys.length -1){
                            //console.log(parseInt(item.new_quantity));
                            //console.log(parseInt(item.groupDiscounts[d].d));
                            groupDiscountSelected = parseFloat(prosforaDetailsCtrl.prosforaDiscountOptions[d].d);
                            break;
                        }else {
                            if (total >= parseInt(prosforaDetailsCtrl.prosforaDiscountOptions[d].q) && total < parseInt(prosforaDetailsCtrl.prosforaDiscountOptions[d+1].q) ){
                                groupDiscountSelected = parseFloat(prosforaDetailsCtrl.prosforaDiscountOptions[d].d);
                                break;
                            }
                        }
                        
                    }


                    var discount_values = [];
                    for(var q=0; q<prosforaDetailsCtrl.prosforaGroupsKeys.length; q++){
                        discount_values[q] = parseInt(prosforaDetailsCtrl.prosforaGroupsKeys[q]);
                    }
                    
                    discount_values = discount_values.sort(function(a,b){ //Array now becomes [41, 25, 8, 7]
                        return b - a
                    });

                    if (total >= discount_values[discount_values.length-1] && prosforaDetailsCtrl.firstAfterMin === false )
                        prosforaDetailsCtrl.firstAfterMin = true;
                    else if (total < discount_values[discount_values.length-1] && prosforaDetailsCtrl.firstAfterMin === true )
                        prosforaDetailsCtrl.firstAfterMin = false;

                    if (total < discount_values[discount_values.length-1] ){
                        if (prosforaDetailsCtrl.firstAfterMin === false){
                            prosforaDetailsCtrl.cart.addToCart(item);
                            prosforaDetailsCtrl.customInvalid = true;
                            prosforaDetailsCtrl.customInvalidMsg = "Η ελάχιστη ποσότητα είναι " + discount_values[discount_values.length-1] + " προιόντα στο σύνολο";
                            return;
                        }
                        
                    }else {
                        prosforaDetailsCtrl.customInvalid = false;
                        prosforaDetailsCtrl.customInvalidMsg = "";
                    }

                    //console.log(groupDiscountSelected);
                    prosforaDetailsCtrl.items.forEach(function(item){
                        item.price_discount = (item.price_katharo * ((100-parseFloat(groupDiscountSelected)) / 100)).toFixed(2);
                        item.item_ekptwsi = groupDiscountSelected;
                    });

                }


                prosforaDetailsCtrl.cart.addToCart(item);
                
                if (prosfora.category === "21"){


                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <300){
                        prosforaDetailsCtrl.discountValue = 5;
                    }else if (totalCost >= 300 && totalCost <= 599) {
                        prosforaDetailsCtrl.discountValue = 7;
                    }else{
                        prosforaDetailsCtrl.discountValue = 9;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }

  				 if (prosfora.category === "28"){


                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <100){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 100 && totalCost <= 299) {
                        prosforaDetailsCtrl.discountValue = 5;
                    }else{
                        prosforaDetailsCtrl.discountValue = 7;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }


             if (prosfora.category === "25"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <100){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 100 ) {
                        prosforaDetailsCtrl.discountValue = 5;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }

  			 if (prosfora.category === "26"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <100){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 100 && totalCost <= 300) {
                        prosforaDetailsCtrl.discountValue = 5;
                    }else if (totalCost >= 301 && totalCost <= 600) {
                        prosforaDetailsCtrl.discountValue = 7;
                    }else {
                        prosforaDetailsCtrl.discountValue = 9;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }

                 if (prosfora.category === "33"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <100){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 100 && totalCost <= 300) {
                        prosforaDetailsCtrl.discountValue = 5;
                    }else {
                        prosforaDetailsCtrl.discountValue = 7;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }
                
                
                if (prosfora.category === "23"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    switch(prosforaDetailsCtrl.userIsMelos){
                        case "0":
                            if (totalCost <100){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else if (totalCost >= 100 && totalCost <= 300) {
                                prosforaDetailsCtrl.discountValue = 5;
                            }else if (totalCost >= 301 && totalCost <= 600) {
                                prosforaDetailsCtrl.discountValue = 7;
                            }else{
                                prosforaDetailsCtrl.discountValue = 9;
                            }
                            break;
                        case "1":
                            if (totalCost <100){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else if (totalCost >= 100 && totalCost <= 300) {
                                prosforaDetailsCtrl.discountValue = 6;
                            }else if (totalCost >= 301 && totalCost <= 600) {
                                prosforaDetailsCtrl.discountValue = 8;
                            }else{
                                prosforaDetailsCtrl.discountValue = 10;
                            }
                            break;
                    }
                    applyDiscount(prosforaDetailsCtrl.discountValue);

                }
 				
 				if (prosfora.category === "29"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    switch(prosforaDetailsCtrl.userIsMelos){
                        case "0":
                            if (totalCost <100){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else if (totalCost >= 100 && totalCost <= 300) {
                                prosforaDetailsCtrl.discountValue = 5;
                            }else if (totalCost >= 301 && totalCost <= 700) {
                                prosforaDetailsCtrl.discountValue = 7;
                            }else{
                                prosforaDetailsCtrl.discountValue = 9;
                            }
                            break;
                        case "1":
                            if (totalCost <100){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else if (totalCost >= 100 && totalCost <= 300) {
                                prosforaDetailsCtrl.discountValue = 6;
                            }else if (totalCost >= 301 && totalCost <= 700) {
                                prosforaDetailsCtrl.discountValue = 8;
                            }else{
                                prosforaDetailsCtrl.discountValue = 10;
                            }
                            break;
                    }
                    applyDiscount(prosforaDetailsCtrl.discountValue);

                }



             if (prosfora.category === "34"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <300){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 300 ) {
                        prosforaDetailsCtrl.discountValue = 7;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }


             if (prosfora.category === "38"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <200){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 200 && totalCost <= 399) {
                        prosforaDetailsCtrl.discountValue = 5;
                    }else if (totalCost >= 400) {
                        prosforaDetailsCtrl.discountValue = 7;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }


 				if (prosfora.category === "30"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    switch(prosforaDetailsCtrl.userIsMelos){
                        case "0":
                            if (totalCost <200){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else if (totalCost >= 200 && totalCost <= 500) {
                                prosforaDetailsCtrl.discountValue = 5;
                            }else if (totalCost >= 501) {
                                prosforaDetailsCtrl.discountValue = 7;
                            }
                            break;
                        case "1":
                            if (totalCost <200){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else if (totalCost >= 200 && totalCost <= 500) {
                                prosforaDetailsCtrl.discountValue = 6;
                            }else if (totalCost >= 501) {
                                prosforaDetailsCtrl.discountValue = 8;
                            }
                            break;
                    }
                    applyDiscount(prosforaDetailsCtrl.discountValue);

                }
                
                       
               if (prosfora.category === "31"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    switch(prosforaDetailsCtrl.userIsMelos){
                        case "0":
                            if (totalCost <100){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else if (totalCost >= 100 && totalCost <= 300) {
                                prosforaDetailsCtrl.discountValue = 5;
                            }else if (totalCost >= 301 && totalCost <= 700) {
                                prosforaDetailsCtrl.discountValue = 6;
                            }else {
                                prosforaDetailsCtrl.discountValue = 13;
                            }
                            break;
                        case "1":
                            if (totalCost <100){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else if (totalCost >= 100 && totalCost <= 300) {
                                prosforaDetailsCtrl.discountValue = 6;
                            }else if (totalCost >= 301 && totalCost <= 700) {
                                prosforaDetailsCtrl.discountValue = 8;
                            }else {
                                prosforaDetailsCtrl.discountValue = 16;
                            }
                            break;
                    }
                    applyDiscount(prosforaDetailsCtrl.discountValue);

                }
                
                
                   if (prosfora.category === "35"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    switch(prosforaDetailsCtrl.userIsMelos){
                        case "0":
                            if (totalCost <300){
                                prosforaDetailsCtrl.discountValue = 5;
                            }else if (totalCost >= 300 && totalCost <= 500) {
                                prosforaDetailsCtrl.discountValue = 7;
                            }else {
                                prosforaDetailsCtrl.discountValue = 9;
                            }
                            break;
                        case "1":
                            if (totalCost <300){
                                prosforaDetailsCtrl.discountValue = 5;
                            }else if (totalCost >= 300 && totalCost <= 500) {
                                prosforaDetailsCtrl.discountValue = 8;
                            }else if (totalCost >= 501 && totalCost <= 1000) {
                                prosforaDetailsCtrl.discountValue = 10;
                            }else {
                                prosforaDetailsCtrl.discountValue = 12;
                            }
                            break;
                    }
                    applyDiscount(prosforaDetailsCtrl.discountValue);

                }
                
             if (prosfora.category === "40"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <100){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 100 && totalCost <= 599) {
                        prosforaDetailsCtrl.discountValue = 5;
                    }else {
                        prosforaDetailsCtrl.discountValue = 7;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }

                 if (prosfora.category === "41"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <100){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 100 && totalCost <= 499) {
                        prosforaDetailsCtrl.discountValue = 7;
                    }else {
                        prosforaDetailsCtrl.discountValue = 9;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }

                 if (prosfora.category === "42"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <150){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else {
                        prosforaDetailsCtrl.discountValue = 5;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }


                 if (prosfora.category === "43"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    if (totalCost <200){
                        prosforaDetailsCtrl.discountValue = 0;
                    }else if (totalCost >= 200 && totalCost <= 599) {
                        prosforaDetailsCtrl.discountValue = 5;
                    }else {                        
                        prosforaDetailsCtrl.discountValue = 7;
                    }

                    applyDiscount(prosforaDetailsCtrl.discountValue);
                }

                
                   if (prosfora.category === "37"){

                    var totalCost = prosforaDetailsCtrl.cart.totalCost();

                    switch(prosforaDetailsCtrl.userIsMelos){
                        case "0":
                            if (totalCost <200){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else {
                                prosforaDetailsCtrl.discountValue = 5;
                            }
                            break;
                        case "1":
                            if (totalCost <200){
                                prosforaDetailsCtrl.discountValue = 0;
                            }else {
                                prosforaDetailsCtrl.discountValue = 7;
                            }
                            break;
                    }
                    applyDiscount(prosforaDetailsCtrl.discountValue);

                }
                
                
            }
            else {
                if (prosforaDetailsCtrl.cart.getById(item.item_id)){
                    prosforaDetailsCtrl.cart.removeFromCart(item.item_id);
                }

                
                if (!prosforaDetailsCtrl.cart.getInvalidById(item.item_id)){
                    if (item.min_quantity !== "0" &&  (item.new_quantity !== undefined || elem["theInput" + item.item_id].$dirty === true && item.new_quantity === undefined)){
                        prosforaDetailsCtrl.cart.invalids.push(item);
                    }
                    
                }

            }
            
        };

        prosforaDetailsCtrl.discountChange = function(){

            applyDiscount(prosforaDetailsCtrl.discountValue);
        };



        function applyDiscount(percent_value, seperateDiscount ){
            var l = prosforaDetailsCtrl.items.length;
            while (l--){
                var ekptwsh = seperateDiscount ? prosforaDetailsCtrl.items[l].item_ekptwsi : percent_value;
                prosforaDetailsCtrl.items[l].price_discount = (prosforaDetailsCtrl.items[l].price_katharo * ((100-ekptwsh) / 100)).toFixed(2);
            }
        };

        /**
         * Group and array of object by a property
         * @param {array}   array of data to group 
         * @param {string}  property to group by
         * @returns {object} object with key and group items
         */
        function groupBy(xs, key) {
            return xs.reduce(function(rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
                return rv;
            }, {});
        };

    }


})();

