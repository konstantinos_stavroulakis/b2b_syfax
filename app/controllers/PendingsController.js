(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .controller('PendingsController', PendingsController);

    function PendingsController($http, $location, $document, $state, $uibModal, orders, prosfora, libProsforaService, ordersService, flashService) {
        var pendingsCtrl = this;

        pendingsCtrl.prosfora = prosfora;
        pendingsCtrl.acceptTerms = false;
        pendingsCtrl.checked = false;
        pendingsCtrl.currentPage = 1;
        pendingsCtrl.pageSize = 10;
        pendingsCtrl.itemsPerPage = "10";
        pendingsCtrl.itemCodeFilter = '';
        pendingsCtrl.itemNameFilter = '';

        pendingsCtrl.cart = {
            items:[],
            invalids:[],
            totalCost: function(){
                var total = 0;
                var cart = this;
                angular.forEach(this.items, function (item) {
                    
                    total += cart.getQuantity(item) * item.price_in_order;
                });
                return +parseFloat(total).toFixed(2);

            },
            getQuantity: function(item){
                return (!angular.isUndefined(item.quantity) && item.quantity !== "" && item.quantity !== null ? item.quantity : 0);
            },
            addToCart: function(item){
                if (!this.isInCart(item)){
                    if (!this.hasMinAndValueZero(item))
                        this.items.push(item);

                }else {
                    // check if to update quantity or not
                    if (this.hasMinAndValueZero(item))
                        this.removeFromCart(item.item_id);
                }
            },
            totalProioda: function(){
                var count = 0;
                for(var i=0; i<this.items.length; i++){
                    count += parseInt(this.items[i].quantity);
                }
                return count;
            },
            hasMinAndValueZero: function(item){
                return parseInt(item.min_quantity) === 0 && parseInt(item.new_quantity) === 0;
            },
            isInCart: function(item){

                if (!this.getById(item.item_id)){
                    return false;
                }else {
                    return true;
                }

            },
            isInInvalids: function(item){

                if (!this.getById(item.item_id)){
                    return false;
                }else {
                    return true;
                }

            },
            removeFromCart: function(item_id){
                for(var i=0 ; i<this.items.length; i++){
                    if (this.items[i].item_id === item_id) {
                        this.items.splice(i, 1);
                        break;
                    }
                }
            },
            removeFromInvalids: function(item_id){
                for(var i=0 ; i<this.invalids.length; i++){
                    if (this.invalids[i].item_id === item_id) {
                        this.invalids.splice(i, 1);
                        break;
                    }
                }
            },
            getById: function(id){
                var theItem = false;

                angular.forEach(this.items, function (item) {
                    if  (item.item_id === id) {
                        theItem = item;
                    }
                });

                return theItem;

            },
            getInvalidById: function(id){
                var theItem = false;

                angular.forEach(this.invalids, function (item) {
                    if  (item.item_id === id) {
                        theItem = item;
                    }
                });

                return theItem;

            }
        };

        pendingsCtrl.orders = orders;
        if (prosfora){
            pendingsCtrl.order = prosfora.items;        

            // do some data initializations before viewing
            var l = pendingsCtrl.order.length;
            while (l--){
                
                if (pendingsCtrl.order[l].quantity > 0){
                    // if item's min quantity is not 0, then set quantity to min quantity and add to cart by default
                    
                    pendingsCtrl.cart.addToCart(pendingsCtrl.order[l]);
                }else {
                    pendingsCtrl.order[l].quantity = 0;
                }
                
            }
            
            // fernw thn lista twn ekptwsewn gia thn kathgoria ths prosforas
            pendingsCtrl.discounts = libProsforaService.getDiscountOpts(parseInt(prosfora.category));

            // an h kathgoria einai h 6 => tote den exw enoia ekptwsh alla seperate. an oxi tote exw eniaia ekptwsh kai epilegw auth p exun ta items
            // here was the bug. we assumed that the first item would have discount. 
            // instead find the first non null item_discount findIndex
            var index = prosfora.items.findIndex(function(val){return val.item_discount !== null;})
            var disc = parseInt(prosfora.category) === 6 ? null : parseInt(prosfora.items[index].item_discount);
            pendingsCtrl.discountValue = disc; 

            // apply discount to items in order
            applyDiscount(pendingsCtrl.discountValue, parseInt(prosfora.category) === 6 ? true : false );

        }

        pendingsCtrl.toggleSelection = function($event, item){
            var checkbox = $event.target;
            pendingsCtrl.acceptTerms = checkbox.checked;
        };

        pendingsCtrl.addItem = function(item, elem, valid){
            var tip = angular.element("#item_tip"+item.item_id)[0];
            
            if (item.quantity >= 100)
                tip.style.display = "block";
            else
                tip.style.display = "none";

            

            
            
            if (valid){
                if (pendingsCtrl.cart.getInvalidById(item.item_id))
                    pendingsCtrl.cart.removeFromInvalids(item.item_id);
                else if (parseInt(item.quantity) === 0)
                    pendingsCtrl.cart.removeFromCart(item.item_id);
                else
                    pendingsCtrl.cart.addToCart(item);
            }
            else {
                if (pendingsCtrl.cart.getById(item.item_id)){
                    pendingsCtrl.cart.removeFromCart(item.item_id);
                }

                if (!pendingsCtrl.cart.getInvalidById(item.item_id)){
                    pendingsCtrl.cart.invalids.push(item);
                }
            }
            
            

            
            
        };

        pendingsCtrl.submitForm = function(theForm, isPending){
            var pendingOrder = (isPending && isPending === true) ? true : false;
            pendingsCtrl.dataLoading = true;
            var sendData = [];
            var l = pendingsCtrl.cart.items.length;
            while (l--){
                sendData.push({
                    item_id: pendingsCtrl.cart.items[l].item_id,
                    min_quantity:pendingsCtrl.cart.items[l].min_quantity,
                    new_quantity:pendingsCtrl.cart.items[l].quantity,
                    price_discount:pendingsCtrl.cart.items[l].price_in_order,
                    price_arxiki:pendingsCtrl.cart.items[l].price_katharo,
                    item_ekptwsi:pendingsCtrl.cart.items[l].item_discount
                });
            }

            console.log(sendData);
            var order_id = $state.params.order_id;
            console.log(order_id);
            
            var ret = ordersService.sendOrder(sendData, order_id, pendingOrder )
            .then(function (data, status, headers, conf) {                    
                if (data.data.status == "success"){

                    if (!pendingOrder){
                        var newOrderId = data.data.result.orderid;
                        $location.path('summary/'+newOrderId).search({submit: 'true'});
                        angular.element('#confirm-submit').modal('hide');
                        $document[0].body.classList.remove('modal-open');
                        angular.element($document[0].getElementsByClassName('modal-backdrop')).remove();
                        angular.element($document[0].getElementsByClassName('modal')).remove();
                    }else {
                        
                        flashService.set("Η προσφορά ενημερώθηκε και βρίσκεται σε κατάσταση αναμονής");
                        $location.path('/');
                        angular.element('#confirm-submit').modal('hide');
                        $document[0].body.classList.remove('modal-open');
                        angular.element($document[0].getElementsByClassName('modal-backdrop')).remove();
                        angular.element($document[0].getElementsByClassName('modal')).remove();

                    }


                }else {

                }
            }).finally(function () {
                prosforaDetailsCtrl.dataLoading = false;
            });

        };



        function applyDiscount(percent_value, seperateDiscount){

            var l = pendingsCtrl.order.length;
            while (l--){
                var ekptwsh = seperateDiscount ? pendingsCtrl.order[l].item_ekptwsi : percent_value;
                pendingsCtrl.order[l].price_in_order = (pendingsCtrl.order[l].price_katharo * ((100-ekptwsh) / 100)).toFixed(2);
                pendingsCtrl.order[l].item_discount = ekptwsh.toString();
            }

        }

        pendingsCtrl.open = function (pending_order_id, parentSelector) {
            var parentElem = parentSelector ? 
            angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: 'myModalContent.html',
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl',
                size: 300,
                appendTo: parentElem,
                resolve: {
                    item: function () {
                        return pending_order_id;
                    }
                }
            });

            modalInstance.result.then(function (val) {
                //$ctrl.test = "test";
                pendingsCtrl.msg = val;
            }, function () {
                console.log('Modal dismissed at: ' + new Date());
            });
        };
        

   }

})();


// https://tc39.github.io/ecma262/#sec-array.prototype.findIndex
if (!Array.prototype.findIndex) {
  Object.defineProperty(Array.prototype, 'findIndex', {
    value: function(predicate) {
     // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw new TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw new TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return k.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return k;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return -1.
      return -1;
    }
  });
}