var myApp = angular.module("syfaxapp",["ui.router", "scrollable-table", 'ui.bootstrap','ngSanitize']);

myApp.config(function($stateProvider, $urlRouterProvider){

    var states = [

        
        { 
            name: 'home', 
            url: '/', 
            templateUrl: 'app/partials/home.html',
            controller:'MainController',
            controllerAs: "mainCtrl",
            onEnter: function($state, authService){
                if(!authService.isAuthed()){
                    $state.go('login');
                }
            },
            data: {
                requireLogin:false
            }
        },

        { 
            name: 'login', 
            url: '/login/:id?', 
            templateUrl: 'app/partials/login.html',
            controller:'LoginController',
            controllerAs: "loginCtrl",
            data: {
                requireLogin:false
            }
            
        },

        { 
            name: 'terms', 
            url: '/terms', 
            templateUrl: 'app/partials/terms.html',
            data: {
                requireLogin:false
            }
            
        },

        

        { 
            name: 'register', 
            url: '/register', 
            templateUrl: 'app/partials/register.html',
            controller:'RegisterController',
            controllerAs: 'registerCtrl',
            data: {
                requireLogin:false
            },
            resolve:{
                doy: function(userService){
                    return userService.getDoyData();
                }
            }
            
        },

        {
            name: 'pendings', 
            url: '/pendings', 
            templateUrl: 'app/partials/pendings.html',
            controller:'PendingsController',
            controllerAs: 'pendingsCtrl' ,
            data: {
                requireLogin:true
            },
            resolve:{
                orders:function( ordersService){
                    return ordersService.getPendingOrders();   
                },
                orderId :function($stateParams){
                    var id = $stateParams.id ? $stateParams.id : null;
                    return id;
                },
                prosfora:function(){
                    return null;
                }
            } 

        },

        {
            name: 'pending_edit',
            url: '/pending_edit/{order_id:int}',
            templateUrl: 'app/partials/pending_details.html',
            controller: 'PendingsController',
            controllerAs: 'pendingsCtrl',
            data: {
                requireLogin:true
            },
            params: {
                order_id: { value: null, squash: true }
            },
            resolve:{
                orders:function(){
                    return null;
                },
                prosfora:function(ordersService, $stateParams){
                    var id = $stateParams.order_id ? $stateParams.order_id : null;
                    return ordersService.getPendingOrders(id);
                }
            }
            
        },

        

        { 
            name: 'orders', 
            url: '/orders', 
            templateUrl: 'app/partials/orders.html',
            controller:'OrdersController',
            controllerAs: 'ordersCtrl' ,
            data: {
                requireLogin:true
            },
            resolve:{
                orders:function( ordersService){
                    return ordersService.getOrders();   
                },
                orderId :function($stateParams){
                    var id = $stateParams.id ? $stateParams.id : null;
                    return id;
                },
                order:function(){
                    return null;
                }
            } 
        },

        {
            name: 'prosfores',
            url: '/prosfores',
            templateUrl: 'app/partials/prosfores.html',
            controller: 'ProsforesController',
            controllerAs: 'prosforesCtrl',
            data: {
                requireLogin:true
            },
            resolve:{
                prosfores:function(prosforesService){
                    return prosforesService.getAllActive();
                }
            }
            
        },

        {
            name: 'view',
            url: '/view/{id:int}',
            templateUrl: 'app/partials/prosfora_details.html',
            controller: 'ProsforaDetailsController',
            controllerAs: 'prosforaDetailsCtrl',
            data: {
                requireLogin:true
            },
            params: {
                id: { value: null, squash: true }
            },
            resolve:{
                prosfora:function(prosforesService, $stateParams){
                    var id = $stateParams.id ? $stateParams.id : null;
                    return prosforesService.getAllActive(id);
                }
            }
            
        },


        {
            name: 'summary',
            url: '/summary/{id:int}',
            templateUrl: 'app/partials/order_summary.html',
            controller: 'OrdersController',
            controllerAs: 'ordersCtrl',
            data: {
                requireLogin:true
            },
            params: {
                id: { value: null, squash: true }
            },
            resolve:{
                
                orders:function(){
                    return null;
                },

                orderId :function($stateParams){
                    var id = $stateParams.id ? $stateParams.id : null;
                    return id;
                },

                order:function(ordersService, $stateParams){
                    var id = $stateParams.id ? $stateParams.id : null;
                    return ordersService.getOrders(id);
                }
            }
            
        },

        


    ];




    // default route
    $urlRouterProvider.otherwise("/");

    // Loop over the state definitions and register them
    states.forEach(function(state) {
        $stateProvider.state(state);
    });


});


//We already have a limitTo filter built-in to angular,
//let's make a startFrom filter
myApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

function authInterceptor(authService, $q) {
  return {
    // automatically attach Authorization header
    request: function(config) {
        var token = authService.getToken();
        if(config.url.indexOf("api") !== -1 && token) {
            config.headers.Authorization = 'Bearer ' + token;
        }
        return config;
    },

    response: function(res) {
        if (res.config.url.indexOf("register") !== -1 || res.config.url.indexOf("api/resources.php") !== -1 || res.config.url.indexOf("api/timologia.php") !==-1){
            // in register we don't save any token
        }else if (res.config.url.indexOf("api") !== -1 && res.data.status !== "error" && res.data.token.jwt) {
            authService.saveToken(res.data.token.jwt);
        }

        return res;
    },
  };
}; 


myApp.run(function ($rootScope, $state, authService, flashService) {

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
        var requireLogin = toState.data.requireLogin;
        if (toState.name === "home"){
            var queue = flashService.getQueue();
            if (queue.length > 0) 
                toParams['currentMessage'] = queue.shift();
            else
                toParams['currentMessage'] = '';
        }

        if (requireLogin && !authService.isAuthed()) {
            event.preventDefault();
            return $state.go('login');
        }
    });
});
myApp.factory('authInterceptor', authInterceptor);
myApp.factory('noCacheInterceptor',["$log","$injector", function($log, $injector){
    var noCacheFolders = ["partials"];

    var shouldApplyNoCache = function (config) {
        // The logic in here can be anything you like, filtering on
        // the HTTP method, the URL, even the request body etc.
        for (var i = 0; i < noCacheFolders.length; i++) {
            var folder = noCacheFolders[i];
            if (config.url.indexOf(folder + "/") !== -1) {
                return true;
            }
        }
        return false;
    };

    var applyNoCache = function (config) {
        if (config.url.indexOf("?") !== -1) {
            config.url += "&";
        } else {
            config.url += "?";
        }
        config.url += "nc=" + new Date().getTime();
    };

    var interceptor = {
        request: function (config) {
            if (shouldApplyNoCache(config)) {
                applyNoCache(config);
            }
            return config;
        }
    };

    return interceptor;
}]);
myApp.config(function($httpProvider) {
  $httpProvider.interceptors.push('authInterceptor');
  $httpProvider.interceptors.push('noCacheInterceptor');
});




var ModalController = function ($scope, $modal) {

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalController,
      resolve: {
        test: function () {
          return 'test variable';
        }
      }
    });

   
  
};




