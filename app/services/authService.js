(function(){
    'use sctrict';

    angular
        .module('syfaxapp')
        .service('authService', authService);
    
    function authService($window) {

        var srvc = this;

        srvc.parseJwt = function (token) {
            var base64Url = token.split('.')[1];
            var base64 = base64Url.replace('-', '+').replace('_', '/');
            return JSON.parse($window.atob(base64));
        };
        
        srvc.saveToken = function (token) {
            $window.localStorage['jwtToken'] = token;
        };
        
        srvc.logout = function () {
            $window.localStorage.removeItem('jwtToken');
        };
        
        srvc.getToken = function () {
            return $window.localStorage['jwtToken'];
        };

        srvc.isUserMelos = function(){
            if (srvc.isAuthed()){
                var token = srvc.getToken();
                if (token) {
                    var params = srvc.parseJwt(token);
                    return params.data ? params.data.is_melos : false;
                }
            }else {
                return false;
            }
        };
        
        srvc.isAuthed = function () {
            var token = srvc.getToken();
            if (token) {
                var params = srvc.parseJwt(token);
                return Math.round(new Date().getTime() / 1000) <= params.exp;
            } else {
                return false;
            }
        };
        
    }

})();