(function () {
    'use strict';

    angular
        .module('syfaxapp')
        .service('userService', userService);

    function userService($http) {
        var userSrvc = this;

        userSrvc.getDoyData = function(){
            return $http.get('api/resources.php',{
                
                params:{
                    type: 'doy'
                }

            }).then(function(data, status, headers, conf){
                return data.data;
            }, function(err_response){
                console.log(err_response);
            });
        };

        userSrvc.login = function(email, password){

            var credentials = btoa(email + ':' + password);
            var authorization = {'Authorization': 'Basic ' + credentials};


            $http.defaults.headers.common['Authorization'] = 'Basic ' + credentials;
            return $http.post('api/login.php');
        };

        userSrvc.register = function(user){
            

            var credentials = btoa(user.username + ':' + user.password);
            var authorization = {'Authorization': 'Basic ' + credentials};

            $http.defaults.headers.common['Authorization'] = 'Basic ' + credentials;
            
            var data = $.param({
                farmacyName: user.farmacyName,
                syfaxCode: user.syfaxCode,
                email:user.email,
                address: user.address,
                afm:user.afm,
                doy:user.doy,
                phone:user.thlefono,
                epaggelma:user.epaggelma
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };


            return $http.post('api/register.php', data, config);


        };
        
    }

})();