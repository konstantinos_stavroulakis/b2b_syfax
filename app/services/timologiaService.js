(function(){
    'use sctrict';

    angular
        .module('syfaxapp')
        .service('timologiaService', timologiaService);


    function timologiaService($http){
        var timologiaService = this;

        timologiaService.getTimologio = function(id){
            
            var downloadRequst = {
                method: 'GET',
                url: 'api/timologia.php?id='+id,
                headers: {
                    'Content-Type': "application/txt",
                    'Accept': "application/txt"
                },
                responseType: 'arraybuffer'
            }

            return $http(downloadRequst);


        };


    }

})();