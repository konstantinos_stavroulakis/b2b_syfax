(function(){
    'use sctrict';

    angular
        .module('syfaxapp')
        .service('prosforesService', prosforesService);


    function prosforesService($http){
        var prosforesSrvc = this;

        prosforesSrvc.getAllActive = function(id){
            return $http.get('api/prosfores.php',{
                
                params:{
                    id: id
                }
            }).then(function(data, status, headers, conf){
                return data.data.result;
            }, function(err_response){
                console.log(err_response);
            });
        }


        prosforesSrvc.sendOrder = function(items, prosforaId, discount, each, pendingOrder){

            var data = $.param({
                items: items,
                prosforaId: prosforaId,
                discount:discount,
                each: each,
                pendingOrder: pendingOrder
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };


            return $http.post('api/update_db.php', data, config)
            
        }
    }

})();