(function(){
    'use sctrict';

    angular
        .module('syfaxapp')
        .service('flashService', flashService);


    function flashService($rootScope){
        var queue = [], currentMessage = '';
  
        
        return {
            getQueue: function(){
                return queue;
            },
            set: function(message) {
                queue.push(message);
            },
            get: function(message) {
                return currentMessage;
            }
        };


    }

})();