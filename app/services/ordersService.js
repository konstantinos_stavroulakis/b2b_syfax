(function(){
    'use sctrict';

    angular
        .module('syfaxapp')
        .service('ordersService', ordersService);


    function ordersService($http){
        var ordersService = this;

        ordersService.getOrders = function(id){
            return $http.get('api/orders.php',{
                
                params:{
                    id: id
                }
            }).then(function(data, status, headers, conf){
                return data.data.result;
            }, function(err_response){
                console.log(err_response);
            });
        };


        ordersService.getPendingOrders = function(id){
            return $http.get('api/orders.php',{
                
                params:{
                    pending: true,
                    id: id
                }

            }).then(function(data, status, headers, conf){
                return data.data.result;
            }, function(err_response){
                console.log(err_response);
            });
        };

        ordersService.cancelOrder = function(pendingOrderId){
            var data = $.param({
                pendingOrderId: pendingOrderId,
                delete:true
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };

            return $http.post('api/orders.php', data, config);
        };

        ordersService.sendOrder = function(items, orderId, isPending){

            var data = $.param({
                items: items,
                orderId: orderId,
                isPending:isPending
            });
        
            var config = {
                headers : {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
            };


            return $http.post('api/orders.php', data, config);

        };


    }

})();