<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta charset="UTF-8">
    <title>ΣΥ.ΦΑ.Χ.</title>
    <link rel="shortcut icon" type="image/x-icon" href="app/assets/images/favicon.png">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=all" rel="stylesheet" type="text/css">
    <link href="app/assets/css/bootstrap.min.css" rel="stylesheet" />
    <?php
        echo '<link href="app/assets/css/styles.css?nc='.rand().'" rel="stylesheet" />';
    ?>
    <link href="app/assets/css/scrollable-table.css" rel="stylesheet" />
    <script type="text/javascript" src="app/assets/scripts/jquery.min.js"></script>
    <!-- add ga account -->
    <script>   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)   })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');    ga('create', 'UA-93098003-1', 'auto');   ga('send', 'pageview');  </script>
</head>

<body ng-app="syfaxapp" ng-controller="MainController as mainCtrl">
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand syfax-logo" ui-sref="home" > 
                    <span><img class="logo" src="app/assets/images/logo.png"/></span>
                    <span class="syfax-title">ΣΥ.ΦΑ.Χ.</span>
                </a>
            </div>


            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <ul class="nav navbar-nav navbar-right" ng-if="mainCtrl.isLoggedIn() === false">
                        <li><a ui-sref="login">Είσοδος </a></li>
                        <li><a ui-sref="register">Εγγραφή </a></li>
                        <li ng-if="mainCtrl.isLoggedIn()"><a ng-click="mainCtrl.logout()">Έξοδος </a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right" ng-if="mainCtrl.isLoggedIn()">
                        <li ng-class="{active: mainCtrl.$state.includes('prosfores')}"><a ui-sref="prosfores">Ενεργές προσφορές </a></li>
                        <li ng-class="{active: mainCtrl.$state.includes('orders')}" class="dropdown">
                            <a class="dropdown-toggle" id="ddOrdersHistory" data-toggle="dropdown" >
                                Οι παραγγελίες μου <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu"> 
                                <li><a ui-sref="orders">Απεσταλμένες</a></li> 
                                <li><a ui-sref="pendings">Σε Αναμονή</a></li> 
                            </ul>
                        </li>
                        <li class="pointer"><a ng-click="mainCtrl.logout()">Έξοδος </a></li>
                    </ul>
                </ul>
            </div>
        </div>
    </nav>
    
    <ui-view></ui-view>
    

    <footer class="footer">
      <div class="container">
        <p class=" pull-left">Ανάπτυξη &amp; Σχεδιασμός <a href="http://www.apphub.gr/" target="blank" class="terms text-muted">Apphub</a></p>  
        <p class=" pull-right"><a ui-sref="terms" class="terms text-muted">Όροι Χρήσης</a></p>
      </div>
    </footer>

    
    <script type="text/javascript" src="app/assets/scripts/bootstrap.min.js"></script>
    <script type="text/javascript" src="node_modules/file-saver/FileSaver.min.js"></script>
    <script src="app/assets/scripts/angular.js"></script>
    <script type="text/javascript" src="node_modules/angular-sanitize/angular-sanitize.min.js"></script>
    <script type="text/javascript" src="node_modules/angular-ui-router/release/angular-ui-router.min.js"></script>
    <script src="app/assets/scripts/ui-bootstrap-tpls-0.14.3.min.js"></script>
    <script type="text/javascript" src="app/directives/angular-scrollable-table.js"></script>

    <?php
        $nc = rand();
        echo '
        <script type="text/javascript" src="app/app.js?nc='.$nc.'"></script>
        <script src="app/services/flashService.js?nc='.$nc.'"></script>
        <script src="app/services/authService.js?nc='.$nc.'"></script>
        <script src="app/services/prosforesService.js?nc='.$nc.'"></script>
        <script src="app/services/ordersService.js?nc='.$nc.'"></script>
        <script src="app/services/userService.js?nc='.$nc.'"></script>
        <script src="app/services/timologiaService.js?nc='.$nc.'"></script>
        <script src="app/services/common.js?nc='.$nc.'"></script>
        <script src="app/controllers/MainController.js?nc='.$nc.'"></script>
        <script src="app/controllers/ProsforesController.js?nc='.$nc.'"></script>
        <script src="app/controllers/LoginController.js?nc='.$nc.'"></script>
        <script src="app/controllers/RegisterController.js?nc='.$nc.'"></script>
        <script src="app/controllers/ProsforaDetailsController.js?nc='.$nc.'"></script>
        <script src="app/controllers/OrdersController.js?nc='.$nc.'"></script>
        <script src="app/controllers/PendingsController.js?nc='.$nc.'?nc='.$nc.'"></script>
        <script src="app/controllers/ModalInstanceCtrl.js"></script>
        <script src="app/directives/directives.js?nc='.$nc.'"></script>
        ';

    ?>
    <script type="text/javascript">

    $(function(){
        $(".dropdown").hover(       
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeIn("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            },
            function() {
                $('.dropdown-menu', this).stop( true, true ).fadeOut("fast");
                $(this).toggleClass('open');
                $('b', this).toggleClass("caret caret-up");                
            }
        );

        
    });
    </script>

</body>

</html>