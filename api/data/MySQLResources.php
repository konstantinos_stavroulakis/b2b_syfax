<?php

require_once 'MySQL.php';
require_once './models/mDoy.php';

class MySQLResources {

    static function getDoyData(){
        $doy = array();
        $db = &MySQL::getInstance();

        $sql = 'SELECT id, onomasia FROM doy';
        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'Doy');
            $doy = $stmt->fetchAll();
           
            $stmt = null;

        }catch (Exception $e) {
            echo $e->getMessage();
        }

        return $doy;
    }
}


?>