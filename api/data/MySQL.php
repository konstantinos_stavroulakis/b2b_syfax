<?php
require ("./config/config.php");

class MySQL {

    // dimiourgia tou instance tis vasis dedomenwn
    static function create() {
        

        /* Database Settings */
        $db_host = db_hostname;		// This is normally set to localhost
        $db_user = db_username;		// MySQL username
        $db_pass = db_password;		// MySQL password
        $db_name = db_name;	// MySQL database name


        $dsn = "mysql:host=$db_host;dbname=$db_name";
        $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            PDO::ATTR_PERSISTENT => true
        );

        try {
            $dbh = new PDO($dsn, $db_user, $db_pass, $options);
            
        }catch (PDOException $e) {
            die("Could not connect to the database $dbname :" . $e->getMessage());
        }

       
        
        return $dbh;
    }
    
    // dimiourgei mia fora to instance tis vasis kai apo ekei kai pera to epistrefei
    static function &getInstance() {
        static $instance;

        if (!is_object($instance)) {
            $instance = MySQL::create();
        }

        return $instance;
    }
}