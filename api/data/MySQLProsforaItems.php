<?php

require_once 'MySQL.php';
require_once './models/mProsforaItem.php';

class MySQLProsforaItems {

    static function getProsforaItems($prosfora_id){
        $response = array();
        $prosfora_items = array();
        $db = &MySQL::getInstance();
        $p_id = (int)$prosfora_id;

        $sql = 'SELECT pi.id, pi.prosfora_id, pi.item_id, pi.min_quantity, pi.item_ekptwsi,
        i.id, i.apothiki, i.item_code, i.item_name, i.barcode, i.price_katharo, i.price_me_fpa, i.fpa_code,fpa.id, fpa.fpa_code, fpa.perigrafi, fpa.pososto 
        FROM prosfora_items as pi 
        join item as i on pi.item_id = i.id            
        join fpa as fpa on i.fpa_code = fpa.fpa_code            
        where pi.prosfora_id = :prosforaid order by i.item_name, pi.min_quantity asc';

        $sql2 = 'SELECT prosfora_id, prosfora_category, prosfora_title, comment, prosfora_type FROM prosfora WHERE prosfora_id = :prosforaid';

        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute(['prosforaid'=>$p_id]);
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'ProsforaItem');
            $prosfora_items = $stmt->fetchAll();
           
            $stmt = null;

            $stmt = $db->prepare($sql2);
            $stmt->execute(['prosforaid'=>$p_id]);
            $prosfora = $stmt->fetchAll();
           
            

            $response['id'] = $p_id;
            $response['category'] = $prosfora[0]['prosfora_category'];
            $response['prosfora_title'] = $prosfora[0]['prosfora_title'];
            $response['prosfora_comment'] =$prosfora[0]['comment'];
            $response['prosfora_type'] = $prosfora[0]['prosfora_type'];
            $response['items'] = $prosfora_items;

        }catch (Exception $e) {
            echo $e->getMessage();
        }


        
        return $response;

    }
}
?>