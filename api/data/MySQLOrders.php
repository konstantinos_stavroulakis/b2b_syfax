<?php

require_once 'MySQL.php';
require_once './models/mOrder.php';
require_once './models/mPendingOrder.php';
require_once './models/mTimologio.php';
require_once './models/mOrderSummaryItem.php';
require_once './models/mPendingOrderSummaryItem.php';



class MySQLOrders {


    static function postPendingOrder($items, $prosforaId, $discount, $each, $pendingOrder, $user_code){

        // if each is 1 then:
        // 1. add discount=0 to orders. else we add the global $discount 
        // 2. add in order_details each item's discount (TODO ADD DISCOUNT TO $items). else add $discount
        
        $db = &MySQL::getInstance();
        $status = '';
        $errMsg = '';

        //We start our transaction.
        $db->beginTransaction();
        
        //We will need to wrap our queries inside a TRY / CATCH block.
        //That way, we can rollback the transaction if a query fails and a PDO exception occurs.
        try{
        
            //Query 1: Attempt to insert the order
            $ekptwsh = (isset($each) && $each == 1) ? 0 : $discount;
            $sql = "INSERT into pending_orders SET prosfora_id=:prosfora_id, user_id=:user_id, order_date=:order_date";
            $stmt = $db->prepare($sql);
            $stmt->execute(['prosfora_id' => $prosforaId, 'user_id' => $user_code, 'order_date' => date("Y-m-d H:i:s")]);
            
            // take order id
            $orderId = $db->lastInsertId();

            $stmt = null;

            // next insert items in order_details
            $sql2 = "INSERT INTO order_details SET 
            pending_order_id=:order_id, item_id=:item_id, quantity=:quantity , price_in_order=:price_in_order, 
            price_arxiki=:price_arxiki, item_discount=:selected_item_discount ";
            $stmt1 = $db->prepare($sql2);
            
            foreach ($items as $item){
                
                $itemQuantity = $item['new_quantity'];

                if($itemQuantity > 0 && $itemQuantity >= $item['min_quantity'] ){

                    $stmt1->execute([
                        'order_id' => $orderId, 
                        'item_id' => $item['item_id'], 
                        'quantity' => $itemQuantity,
                        'price_in_order' => $item['price_discount'],
                        'price_arxiki' => $item['price_arxiki'],
                        'selected_item_discount' => ($each == 1 ? $item['item_ekptwsi'] : $discount)
                    ]);

                }//end if
            }
            
            

            //We've got this far without an exception, so commit the changes.
            $db->commit();

            $status = "success";
            
        } 
        //Our catch block will handle any exceptions that are thrown.
        catch(PDOException $e){
            //An exception has occured, which means that one of our database queries
            //failed.

            //Print out the error message.
            $status = "error";
            $errMsg = $e->getMessage();
            //Rollback the transaction.
            $db->rollBack();
        }



       $data['status'] = $status;
       $data['errMsg'] = $errMsg;
       $data['orderid'] = $orderId;
       
       return $data;

    }

    static function postOrder($items, $prosforaId, $discount, $each, $user_code){

        // if each is 1 then:
        // 1. add discount=0 to orders. else we add the global $discount 
        // 2. add in order_details each item's discount (TODO ADD DISCOUNT TO $items). else add $discount
        
        $db = &MySQL::getInstance();
        $status = '';
        $errMsg = '';

        //We start our transaction.
        $db->beginTransaction();
        
        //We will need to wrap our queries inside a TRY / CATCH block.
        //That way, we can rollback the transaction if a query fails and a PDO exception occurs.
        try{
        
            //Query 1: Attempt to insert the order
            $ekptwsh = (isset($each) && $each == 1) ? 0 : $discount;
            $sql = "INSERT into orders SET prosfora_id=:prosfora_id, user_id=:user_id, order_date=:order_date, discount=:discount";
            $stmt = $db->prepare($sql);
            $stmt->execute(['prosfora_id' => $prosforaId, 'user_id' => $user_code, 'order_date' => date("Y-m-d H:i:s"), 'discount' => $discount]);
            
            // take order id
            $orderId = $db->lastInsertId();

            $stmt = null;

            // next insert items in order_details
            $sql2 = "INSERT INTO order_details SET 
            order_id=:order_id, item_id=:item_id, quantity=:quantity , price_in_order=:price_in_order, 
            price_arxiki=:price_arxiki, item_discount=:selected_item_discount ";
            $stmt1 = $db->prepare($sql2);
            
            foreach ($items as $item){
                
                $itemQuantity = $item['new_quantity'];

                if($itemQuantity > 0 && $itemQuantity >= $item['min_quantity'] ){

                    $stmt1->execute([
                        'order_id' => $orderId, 
                        'item_id' => $item['item_id'], 
                        'quantity' => $itemQuantity,
                        'price_in_order' => $item['price_discount'],
                        'price_arxiki' => $item['price_arxiki'],
                        'selected_item_discount' => ($each == 1 ? $item['item_ekptwsi'] : $discount)
                    ]);

                }//end if
            }
            
            

            //We've got this far without an exception, so commit the changes.
            $db->commit();

            $status = "success";
            
        } 
        //Our catch block will handle any exceptions that are thrown.
        catch(PDOException $e){
            //An exception has occured, which means that one of our database queries
            //failed.

            //Print out the error message.
            $status = "error";
            $errMsg = $e->getMessage();
            //Rollback the transaction.
            $db->rollBack();
        }



       $data['status'] = $status;
       $data['errMsg'] = $errMsg;
       $data['orderid'] = $orderId;
       
       return $data;

    }

    static function getUserOrders($user_code){

        $orders = array();
        $db = &MySQL::getInstance();

        $sql = 'SELECT id, o.prosfora_id, p.prosfora_title, order_date, count(tim.orderId) as timologia_count ,status 
        FROM 
        orders as o inner join prosfora as p on o.prosfora_id = p.prosfora_id
        left join timologia_details as tim on o.id = tim.orderId
        
        WHERE user_id=:user_code
        GROUP by o.id';
        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute(['user_code' => $user_code]);
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'Order');
            $orders = $stmt->fetchAll();
           
            $stmt = null;

        }catch (Exception $e) {
            echo $e->getMessage();
        }


        return $orders;

    }

    static function getUserPendingOrders($user_code){

        $orders = array();
        $db = &MySQL::getInstance();

        $sql = 'SELECT po.id, po.prosfora_id, p.prosfora_title, po.order_date 
        FROM pending_orders as po inner join prosfora as p on po.prosfora_id = p.prosfora_id
        WHERE user_id=:user_code';
        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute(['user_code' => $user_code]);
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'PendingOrder');
            $orders = $stmt->fetchAll();
           
            $stmt = null;

        }catch (Exception $e) {
            echo $e->getMessage();
        }


        return $orders;

    }

    

    static function getOrderTimologia($order_id){
        $timologia = array();
        $db = &MySQL::getInstance();

        $sql = 'SELECT text FROM timologia_details WHERE orderId=:orderId';

        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute(['orderId' => (int)$order_id]);
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'Timologio');
            $timologia = $stmt->fetchAll();
           
            $stmt = null;

        }catch (Exception $e) {
            echo $e->getMessage();
        }


        return $timologia;
    }

    static function getTimologiaFile($timologia){
        $text = '';
        if (isset($timologia)){

            foreach($timologia as $tim){
                $text .= $tim->text."\r\n";
            }
        }
        return iconv("UTF-8", "Windows-1253//IGNORE", $text); 
    }


    static function insertPendingDetailsItems($items, $pending_order_id, $isPending, $user_code){

        $db = &MySQL::getInstance();
        $status = '';
        $errMsg = '';
        $orderId = NULL;
        $ret = array();
        $isPendingValue = json_decode($isPending);


        //We start our transaction.
        $db->beginTransaction();
        
        //We will need to wrap our queries inside a TRY / CATCH block.
        //That way, we can rollback the transaction if a query fails and a PDO exception occurs.
        try{
        
            if ($isPendingValue == FALSE){
                
                $sql = 'INSERT INTO orders (prosfora_id, user_id, order_date) 
                        (SELECT prosfora_id, user_id, order_date FROM pending_orders WHERE user_id=:user_code and  id =:pendingOrderId)';
                
                $stmt1 = $db->prepare($sql);
                $stmt1->execute(['user_code' => $user_code, 'pendingOrderId' => $pending_order_id]);
                // take order id
                $orderId = $db->lastInsertId();
                $ret['orderid'] = $orderId;


                $sql_del = 'DELETE FROM pending_orders WHERE user_id=:user_code and id=:pendingOrderId';
                $stmt2 = $db->prepare($sql_del);
                $stmt2->execute(['user_code' => $user_code, 'pendingOrderId' => $pending_order_id]);


            }




            // next insert items in order_details
            $sql2 = "INSERT INTO order_details SET 
            pending_order_id=:pending_order_id,
            order_id=:order_id,
            item_id=:item_id, 
            quantity=:quantity , 
            price_in_order=:price_in_order, 
            price_arxiki=:price_arxiki, 
            item_discount=:item_discount ";

            $stmt1 = $db->prepare($sql2);
            
            foreach ($items as $item){
                
                $stmt1->execute([
                    'pending_order_id' => $isPendingValue == TRUE ? $pending_order_id : NULL,
                    'order_id'=> $isPendingValue == FALSE ? $orderId : NULL,
                    'item_id' => $item['item_id'], 
                    'quantity' => $item['new_quantity'],
                    'price_in_order' => $item['price_discount'],
                    'price_arxiki' => $item['price_arxiki'],
                    'item_discount' => $item['item_ekptwsi']
                ]);
            }
            
            

            //We've got this far without an exception, so commit the changes.
            $db->commit();

            $status = "success";
            $ret['status'] = $status;

        }//Our catch block will handle any exceptions that are thrown.
        catch(PDOException $e){
            //An exception has occured, which means that one of our database queries
            //failed.

            //Print out the error message.
            $status = "error";
            $ret['status'] = $status;
            $errMsg = $e->getMessage();
            $ret['errMsg'] = $errMsg;
            //Rollback the transaction.
            $db->rollBack();
        }


        return $ret;

    }

    static function deletePendingOrder($pending_order_id, $user_code){
        $ret = FALSE;
        $db = &MySQL::getInstance();
        try {
            $sql = 'DELETE FROM pending_orders where id=:pending_order_id and user_id=:user_id';
            $stmt = $db->prepare($sql);
            $stmt->execute(['pending_order_id' => $pending_order_id, 'user_id' => $user_code]);
            $deletedRows = $stmt->rowCount();
            $ret['success'] = TRUE;
        }catch (Exception $e) {
            $ret['success']=FALSE;
            $ret['msg'] = $e->getMessage();
        }

        return $ret;
    }

    static function deletePendingDetailsItems($pending_order_id){
        $orderItems = array();
        $db = &MySQL::getInstance();
        $success = FALSE;

        //We start our transaction.
        $db->beginTransaction();
    
        $sql = 'SELECT COUNT(*) FROM order_details where pending_order_id=:pending_order_id';  

        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute(['pending_order_id' => $pending_order_id]);
            $number_of_rows = $stmt->fetchColumn();
            $stmt = null;
            if ($number_of_rows >0){
                $sql = 'DELETE FROM order_details where pending_order_id=:pending_order_id';
                $stmt = $db->prepare($sql);
                $stmt->execute(['pending_order_id' => $pending_order_id]);
                $deletedRows = $stmt->rowCount();
                if ($deletedRows > 0 && $deletedRows == $number_of_rows){

                    //We've got this far without an exception, so commit the changes.
                    $db->commit();
                    $success = TRUE;
                }else {
                    $db->rollBack();
                    $success = FALSE;
                }
            }else{
                $success = FALSE;
            }
           

        }catch (Exception $e) {
            echo $e->getMessage();
            $db->rollBack();
            $success = FALSE;
        }


        return $success;

    
    }

    static function getUserPendingOrderDetails($pending_order_id){

        $orderItems = array();
        $db = &MySQL::getInstance();

        
    
            $sql = 'SELECT 
                i.item_code as item_code, i.id as item_id ,
                i.item_name as item_name, i.price_katharo as price_katharo, pi.item_ekptwsi as item_ekptwsi, pi.min_quantity as min_quantity, 
                od.item_discount as item_discount, od.price_arxiki as price_arxiki,od.price_in_order as price_in_order,od.quantity as quantity

                from prosfora_items as pi
                left join order_details as od on od.item_id = pi.item_id and od.pending_order_id=:order_id
                join item as i on i.id = pi.item_id
                join pending_orders as po on po.id=:order_id
                where pi.prosfora_id = po.prosfora_id
                order by i.item_name asc';

        $sql2 = 'SELECT p.prosfora_id, p.prosfora_category FROM prosfora as p JOIN pending_orders as po on po.prosfora_id = p.prosfora_id WHERE po.id =:order_id';

        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute(['order_id' => $pending_order_id]);
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'PendingOrderSummaryItem');
            $orderItems['items'] = $stmt->fetchAll();
           
            $stmt = null;

            $stmt = $db->prepare($sql2);
            $stmt->execute(['order_id'=>$pending_order_id]);
            $prosfora = $stmt->fetchAll();
            $orderItems['category'] = $prosfora[0]['prosfora_category'];
           

        }catch (Exception $e) {
            echo $e->getMessage();
        }


        return $orderItems;

    }


    static function getOrderSummary($order_id){
        $orderItems = array();
        $db = &MySQL::getInstance();

        
    
        $sql = 'SELECT
            od.id,od.order_id,od.item_id,od.quantity,
            od.price_in_order, od.price_arxiki, i.id,i.apothiki,i.item_code,i.item_name, i.barcode, 
            i.item_min_quantity, i.price_katharo, i.price_me_fpa, i.fpa_code
            FROM order_details as od JOIN  item as i on od.item_id = i.id  WHERE order_id=:order_id';  

        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute(['order_id' => $order_id]);
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'OrderSummaryItem');
            $orderItems = $stmt->fetchAll();
           
            $stmt = null;

        }catch (Exception $e) {
            echo $e->getMessage();
        }


        return $orderItems;
    }
}

?>