<?php
require_once("MySQL.php");
require_once("./models/mUser.php");
require_once("./config/secret.php");
require_once('vendor/autoload.php');
require 'vendor/phpmailer/phpmailer/PHPMailerAutoload.php';
use \Firebase\JWT\JWT; 

class MySQLUser {
    
    
    static function sendActivationSuccessToUser($email){

        $mail_response = array();
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        $mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'b2b.syfachanion@gmail.com';                 // SMTP username
        $mail->Password = 'syfax@123';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom('b2b.syfachanion@gmail.com', 'ΣΥ.ΦΑ.Χ B2B');
        $mail->addAddress($email);     // Add a recipient
        $mail->addBCC("b2b@syfachanion.com");

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'ΣΥ.ΦΑ.Χ B2B  **Ενεργοποίηση νέου χρήστη**';
        $mail->Body    = "<h3>Η ενεργοποίησή σας στο σύστημα ΣΥ.ΦΑ.Χ B2B έγινε επιτυχώς </h3>
        <p>Μπορείτε να συνδεθείτε στο σύστημα συμπληρώνοντας τα στοιχεία που δώσατε κατά την εγγραφή σας στην παρακάτω διεύθυνση:<br>
        <a href='http://www.b2b.syfachanion.com/#/login/'>http://www.b2b.syfachanion.com/#/login/</a></p>
        <p>Ευχαριστούμε</p>
        <p>Ο Διαχειριστής της εφαρμογής</p>
        
        ";

        
        if(!$mail->send()) {
        
            $mail_response['success'] = false;
            $mail_response['msg'] = 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            $mail_response['success'] = true;
            $mail_response['msg'] = 'Message has been sent';
        }

        return $mail_response;

    }
    
    static function sendMailForNewUserActivation($farmacyName, $syfaxCode, $email, $token) {

       
        $mail_response = array();
        $mail = new PHPMailer;
        $mail->CharSet = 'UTF-8';
        //$mail->SMTPDebug = 3;                               // Enable verbose debug output

        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'b2b.syfachanion@gmail.com';                 // SMTP username
        $mail->Password = 'syfax@123';                           // SMTP password
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Port = 587;                                    // TCP port to connect to

        $mail->setFrom('b2b.syfachanion@gmail.com', 'ΣΥ.ΦΑ.Χ B2B');
        $mail->addAddress('b2b@syfachanion.com');     // Add a recipient
        $mail->addBCC('stavroulakis06157@gmail.com');               // Name is optional

        $mail->isHTML(true);                                  // Set email format to HTML

        $mail->Subject = 'ΣΥ.ΦΑ.Χ B2B  **Ενεργοποίηση νέου χρήστη**';
        $mail->Body    = "<h3>Νέος χρήστης στο σύστημα ΣΥ.ΦΑ.Χ B2B</h3><p>
        Ο χρήστης με επωνυμία: <strong>". $farmacyName."</strong> και Κωδικό Φαρμακείου: <strong>".$syfaxCode."</strong> και email: <strong>".$email."</strong> πραγματοποίησε εγγραφή
        στο σύστημα ΣΥ.ΦΑ.Χ B2B.</p>
        <p>Για να τον ενεργοποιήσετε πατήστε στον παρακάτω σύνδεσμο:<br>
        <a href='http://b2b.syfachanion.com/api/activate.php?u=".$email."&tok=".$token."'>http://b2b.syfachanion.com/api/activate.php?u=".$email."&tok=".$token."</a></p>
        <p>Κάνοντας κλικ στον παραπάνω σύνδεσμο, ο χρήστης θα ενημερωθεί με σχετικό email και θα μπορεί να συνδεθεί στο σύστημα με τα στοιχεία του</p> 
        ";

        
        if(!$mail->send()) {
        
            $mail_response['success'] = false;
            $mail_response['msg'] = 'Mailer Error: ' . $mail->ErrorInfo;
        } else {
            $mail_response['success'] = true;
            $mail_response['msg'] = 'Message has been sent';
        }

        return $mail_response;

    }

    static function activateUser($email, $token){
        $db = & MySQL::getInstance();
        $response = array();

        if (!empty($email) && !empty($token)){

            $sql = "UPDATE users SET active=1 WHERE email=:email AND token=:token AND active=:active";
            try {

                $stmt = $db->prepare($sql);
                $result = $stmt->execute(
                    [
                        'email' => $email, 'token' => $token, 'active' => 3
                    ]
                );
                
                //if the row was updated redirect the user
                if($stmt->rowCount() == 1){
                    $response['success'] = true;
                    $response['msg'] = 'Ο χρήστης με email:'.$email.' ενεργοποήθηκε επιτυχώς';
                    $response['email'] = $email;
                }else {
                    $response['success'] = false;
                    $response['msg'] = 'Η ενεργοποίηση του χρήστη με email:'.$email.' απέτυχε';
                }

            }catch (Exception $e){
                $response['success'] = false;
                $response['msg'] = 'Η ενεργοποίηση του χρήστη με email:'.$email.' απέτυχε. Σφάλμα:'.$e->getMessage().'.';
            }

        }else {
            $response['success'] = false;
            $response['msg'] = 'Τα στοιχεία ενεργοποίησης δεν είναι έγκυρα';
        }

        return $response;
    }

    static function createUser($username, $password, $farmacyName, $syfaxCode, $email, $address, $afm, $doy, $phone, $epaggelma ) {
        $db = & MySQL::getInstance();
        $result;
        $response = array();
        $mailSentToAdmin = false;

        # TODO

        /*
        * Check for user data validation. If user is valid procceed
        */

        
        //Query 1: Attempt to insert the order
        $token = md5(openssl_random_pseudo_bytes(32));
        $sql = "INSERT into users SET user_code=:user_code, username=:username, password=:password, email=:email, name=:farmacy_name, 
        address=:address, afm=:afm, doy=:doy, phone=:phone, epaggelma=:epaggelma,
        token=:token, active=:active";
        

        try {
            $password_hash = password_hash($password, PASSWORD_BCRYPT);
            $stmt = $db->prepare($sql);
            $result = $stmt->execute(
                [
                    'user_code' => $syfaxCode, 'username' => $username, 'password' => $password_hash, 
                    'email' => $email, 'farmacy_name' => $farmacyName, 
                    'address' => $address, 'afm' => $afm, 'doy' => $doy, 'phone' => $phone, 'epaggelma' => $epaggelma,
                    'token' => $token, 'active' => 3
                ]
            );

        }catch (Exception $e){
            echo $e->getMessage();
        }

        $response['status'] = isset($result) ? "success" : "error";

        // if registration successfull send mail to admin for verification
        if ($response['status'] == "success"){
            $mailSentToAdmin = self::sendMailForNewUserActivation($farmacyName,$syfaxCode,$email,$token);
        }


        
        return $response;
        
    }

    static function loginUser($email, $password) {
        $db = & MySQL::getInstance();
        $response = array();


        $sql = 'SELECT id, name, user_code, username, email, password, active, melos FROM users WHERE email = :email';
        try {
            

            $stmt = $db->prepare($sql);
            $stmt->execute(['email' => $email]);
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
            $user = $stmt->fetch();
            $stmt = null;

        }catch (Exception $e) {
            echo $e->getMessage();
        }

        $valid = false;
        if (password_verify($password, $user->password)) {
            $valid = true;
        } else {
            $valid = false;
        }

        if(isset($user) && $valid ) {
            if ($user->active == 1){

                $response['status'] = "success";
                $response['message'] = "Logged in successfully";

                $tokenId    = base64_encode(mcrypt_create_iv(32));
                $issuedAt   = time();
                //$notBefore  = $issuedAt + 10;  //Adding 10 seconds
                

                $expire     = $issuedAt + strtotime("30 minutes", $issuedAt); //will expire after 30 minutes
                $serverName = 'http://localhost//'; /// set your domain name 

                
                /*
                    * Create the token as an array
                    */
                $data = [
                    'iat'  => $issuedAt,         // Issued at: time when the token was generated
                    'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
                    'iss'  => $serverName,       // Issuer
                    //'nbf'  => $notBefore,        // Not before
                    'exp'  => $expire,           // Expire
                    'data' => [                  // Data related to the logged user you can set your required data
                        'id'   => $user->id, // id from the users table
                        'name' => $user->name, //  name
                        'email' => $user->email,
                        'user_code' => $user->user_code,
                        'is_melos' => $user->melos
                    ]
                ];

                $secretKey = base64_decode(SECRET_KEY);
                /// Here we will transform this array into JWT:
                $jwt = JWT::encode(
                            $data, //Data to be encoded in the JWT
                            $secretKey, // The signing key
                            ALG 
                        ); 
                $unencodedArray = ['jwt' => $jwt];
                $response['token'] = $unencodedArray;

                
            }else if ($user->active == 2) {
                        
                $response['status'] = "error";
                $response['message'] = "Ο λογαριασμός σας είναι προσωρινά εκτός λειτουργίας";

            }else {

                $response['status'] = "error";
                $response['message'] = "Ο λογαριασμός σας πρέπει να ενεργοποιηθεί από τον διαχειριστή του συστήματος";

            }
            
        }else {
            $response['status'] = "error";
            $response['message'] = "Δεν βρέθηκε ο χρήστης για τα στοιχεία που δώσατε. Το όνομα χρήστη ή ο κωδικός δεν είναι σωστά";

        }

        return $response;
    }
}