<?php

require_once 'MySQL.php';
require_once './models/mProsfora.php';


class MySQLProsfora {


    static function getAllActiveProsfores(){
        $prosfores = array();
        $db = &MySQL::getInstance();

        $sql = 'SELECT prosfora_id, prosfora_date, prosfora_title, prosfora_category FROM prosfora 
        WHERE  DATE(NOW()) <= valid_until
        order by prosfora_date desc';
        try {
            
            $stmt = $db->prepare($sql);
            $stmt->execute();
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'Prosfora');
            $prosfores = $stmt->fetchAll();
           
            $stmt = null;

        }catch (Exception $e) {
            echo $e->getMessage();
        }


        return $prosfores;

    }
}

?>