<?php

//header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization');
header('Content-Type: application/json');

require_once('vendor/autoload.php');
use \Firebase\JWT\JWT; 
require_once("./config/secret.php");
require_once('data/MySQLOrders.php');


$headers = array();
$prosfora_id = null;
$authHeader = null;

if (isset($_GET["id"])){
    $prosfora_id = $_GET["id"];
}


function hasAuthorizationBearer($headers){
    if (isset($headers['Authorization'])){
        return true;
    }else {
        return false;
    }
}

foreach (getallheaders() as $name => $value) {
    $headers[$name] = $value;
}


if (function_exists('apache_request_headers')) {
    $allHeaders = apache_request_headers();
    if (isset($allHeaders['Authorization'])) {
        $authHeader =$allHeaders['Authorization'];
    }
}




if (hasAuthorizationBearer($headers)){
    try {
        $response = array();
        $secretKey = base64_decode(SECRET_KEY);
        list($jwt) = sscanf($headers['Authorization'], 'Bearer %s');
        if ($jwt){
            try {
                $secretKey = base64_decode(SECRET_KEY);
                $token = JWT::decode($jwt, $secretKey, array('HS512'));



                if ((isset($_POST['pendingOrder']) && json_decode($_POST['pendingOrder']) == TRUE)){
                    $data = MySQLOrders::postPendingOrder($_POST['items'],$_POST['prosforaId'],$_POST['discount'], $_POST['each'], $_POST['pendingOrder'], $token->data->user_code);
                }else{
                    $data = MySQLOrders::postOrder($_POST['items'],$_POST['prosforaId'],$_POST['discount'], $_POST['each'], $token->data->user_code);
                }
                


         

                

                //echo  "{'status' : 'success' ,'data':".json_encode($DecodedDataArray)." }";
                $response['status'] = "success";
                $response['token'] = $jwt;
                $response['result'] = $data;

                echo json_encode($response);
                

            }catch (Firebase\JWT\ExpiredException $e){
                header('HTTP/1.0 401 Unauthorized');
                echo json_encode($e->getMessage());
               
            }catch (Firebase\JWT\BeforeValidException $e){
                header('HTTP/1.0 401 Unauthorized');
                echo json_encode($e->getMessage());
            }
            catch (Exception $e){
                
            }
        }

    } catch (Exception $e) {
        echo "{'status' : 'error' ,'msg':'Unauthorized'}";
        die();
    }
}else{
    echo "{'status' : 'error' ,'msg':'Unauthorized'}";

}





?>