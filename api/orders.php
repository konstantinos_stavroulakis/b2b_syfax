<?php

//header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization');
header('Content-Type: application/json');

require_once('vendor/autoload.php');
use \Firebase\JWT\JWT; 
require_once("./config/secret.php");

require_once('data/MySQLOrders.php');
require_once('data/MySQLProsforaItems.php');



$headers = array();
$order_id = null;
$authHeader = null;

$requestType = $_SERVER['REQUEST_METHOD'];

if ($requestType == 'GET' && isset($_GET["id"])){
    $order_id = $_GET["id"];
}


function hasAuthorizationBearer($headers){
    if (isset($headers['Authorization'])){
        return true;
    }else {
        return false;
    }
}

foreach (getallheaders() as $name => $value) {
    $headers[$name] = $value;
}


if (function_exists('apache_request_headers')) {
    $allHeaders = apache_request_headers();
    if (isset($allHeaders['Authorization'])) {
        $authHeader =$allHeaders['Authorization'];
    }
}


function handle_delete_request($pendingOrderId, $user_code){
    $data = array();
    if (isset($pendingOrderId)){
        $data = MySQLOrders::deletePendingOrder($pendingOrderId, $user_code);
    }else {
        $data['success'] = false;
    }

    return $data;

}

function handle_get_request($order_id, $token){
    if (isset($_GET['pending']) && json_decode($_GET['pending']) == true){
        if (isset($order_id)){
            $data = MySQLOrders::getUserPendingOrderDetails($order_id);
        }else{
            $data = MySQLOrders::getUserPendingOrders($token->data->user_code);
        }

    }else{
        if (isset($order_id)){
            $data = MySQLOrders::getOrderSummary($order_id);
        }else {
            $data = MySQLOrders::getUserOrders($token->data->user_code);
        }
    }

    return $data;
}

function handle_post_request($post_data, $user_code){

    //$ret = FALSE;

    $order_id = $post_data['orderId'];
    $isPending = $post_data['isPending'];
    $items = $post_data['items'];
    $ret = array();

    // delete all previous items
    $deleted = MySQLOrders::deletePendingDetailsItems($order_id);
    if (isset($deleted) && $deleted == TRUE){
        // old items deleted successfully

        // go to insert new
        $ret = MySQLOrders::insertPendingDetailsItems($items, $order_id, $isPending, $user_code);
        if ($ret['status'] == "error"){
            //$ret = FALSE;
        }else{
            //$ret = TRUE;
        }

    }else {

        $ret['status'] = "error";
        $ret['errMsg'] = "Η ενημέρωση/αποθήκευση της παραγγελίας απέτυχε";
    }

    return $ret;
}




if (hasAuthorizationBearer($headers)){
    try {
        $response = array();
        $secretKey = base64_decode(SECRET_KEY);
        list($jwt) = sscanf($headers['Authorization'], 'Bearer %s');
        if ($jwt){
            try {
                $secretKey = base64_decode(SECRET_KEY);
                $token = JWT::decode($jwt, $secretKey, array('HS512'));


                //Switch statement
                switch ($requestType) {
                    case 'POST':
                        if (isset($_POST['delete']) && json_decode($_POST['delete']) == true){
                            $data = handle_delete_request($_POST['pendingOrderId'], $token->data->user_code);  
                        }else{
                            $data = handle_post_request($_POST, $token->data->user_code);
                        }
                        
                        break;
                    case 'GET':
                        $data = handle_get_request($order_id, $token);
                        break;
                   
                    default:
                    //request type that isn't being handled.
                    break;
                }



                

                

                //echo  "{'status' : 'success' ,'data':".json_encode($DecodedDataArray)." }";
                $response['status'] = "success";
                $response['token'] = $jwt;
                $response['result'] = $data;

                echo json_encode($response);
                

            }catch (Firebase\JWT\ExpiredException $e){
                header('HTTP/1.0 401 Unauthorized');
                echo json_encode($e->getMessage());
               
            }catch (Firebase\JWT\BeforeValidException $e){
                header('HTTP/1.0 401 Unauthorized');
                echo json_encode($e->getMessage());
            }
            catch (Exception $e){
                
            }
        }

    } catch (Exception $e) {
        echo "{'status' : 'error' ,'msg':'Unauthorized'}";
        die();
    }
}else{
    echo "{'status' : 'error' ,'msg':'Unauthorized'}";

}





?>