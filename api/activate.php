<?php

require_once('data/MySQLUser.php');


$user_email = $_GET['u'];
$token = $_GET['tok'];

$response = MySQLUser::activateUser($user_email, $token);

if ($response['success'] == true){
    
    $userSent = MySQLUser::sendActivationSuccessToUser($response['email']);
    if ($userSent['success'] == true){
        echo "<p>".$response['msg']." και ο χρήστης έχει ενημερωθεί με σχετικό email</p>";
        
    }else {
        echo "<p>".$response['msg'].". Η αποστολή ενημερωτικού email στο χρήστη απέτυχε</p>";
    }


}else{
    echo $response['msg'];
}


?>